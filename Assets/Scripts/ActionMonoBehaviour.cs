﻿using DefaultNamespace;
using UnityEngine;
using Zenject;

public abstract class ActionMonoBehaviour : MonoBehaviour, IActionChange
{
    [Inject]
    private SignalBus m_signalBus;
    
    protected virtual void Awake()
    {
        m_signalBus.Subscribe<ActionSignal>(actionSignal => OnActionChange(actionSignal.HandedType, actionSignal.ActionType));
    }
    
    protected virtual void OnDestroy()
    {
        m_signalBus.Unsubscribe<ActionSignal>(actionSignal => OnActionChange(actionSignal.HandedType, actionSignal.ActionType));
    }

    public abstract void OnActionChange(HandedType handedType, ActionType actionType);
}