﻿namespace DefaultNamespace
{
    public interface IActionChange
    {
        void OnActionChange(HandedType handedType, ActionType actionType);
    }
}