﻿using UnityEngine;
using Zenject;

public class TestMonoInstaller : MonoInstaller
{
    public override void InstallBindings()
    {
        Container.Bind<string>().FromInstance("Zack");
        Container.Bind<Greeter>().AsSingle().NonLazy();
        Container.Bind<Enemy>().AsSingle().NonLazy();
        Container.BindInterfacesAndSelfTo<Player>().AsSingle().NonLazy();
        Container.Bind<PCHand>().WithId(HandedType.Left).FromInstance(new PCHand(HandedType.Left));
        Container.Bind<PCHand>().WithId(HandedType.Right).FromInstance(new PCHand(HandedType.Right));
        Container.Bind<Finger>().WithId(HandedType.Right).FromInstance(new Finger(HandedType.Right));
    }
}

public class Greeter
{
    public Greeter(string msg, IAttack attack)
    {
        attack.ShowDamage();
    }
}

public class Player : IAttack
{
    public readonly string Name;

    public Player(string name)
    {
        Name = name;
    }

    public void ShowDamage()
    {
        Debug.Log("Show Damage");
    }
}

public class Enemy
{
    private Player m_player;

    public Enemy(Player player)
    {
        m_player = player;
    }
}

public interface IAttack
{
    void ShowDamage();
}

public class PCHand : IHand
{
    public PCHand(HandedType handedType)
    {
        HandedType = handedType;
    }

    public HandedType HandedType { get; }
}

public class VRHand : IHand
{
    public VRHand(HandedType handedType)
    {
        HandedType = handedType;
    }

    public HandedType HandedType { get; }
}

public interface IHand
{
    HandedType HandedType { get; }
}

public class Finger
{
    public HandedType HandedType;
    
    
    public Finger(HandedType handedType)
    {
        HandedType = handedType;
    }
}

public enum HandedType
{
    Left,
    Right
}
