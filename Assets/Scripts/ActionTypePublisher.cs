﻿using UnityEngine;
using Zenject;

namespace DefaultNamespace
{
    public class ActionTypePublisher : MonoBehaviour
    {
        [Inject]
        private SignalBus m_signalBus;
        
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                m_signalBus.Fire(new ActionSignal { HandedType = HandedType.Right, ActionType = ActionType.Click});
            }
        }
    }
}