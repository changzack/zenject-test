﻿using UnityEngine;
using Zenject;

public class PlayerController : ActionMonoBehaviour
{
    private Player m_player;
    private IAttack m_attack;
    private IHand m_leftHand;
    private IHand m_rightHand;

    [Inject]
    public void Construct(Player player, IAttack attack, [Inject(Id = HandedType.Left)]IHand leftHand, [Inject(Id = HandedType.Right)]IHand rightHand)
    {
        m_player = player;
        m_attack = attack;
        m_leftHand = leftHand;
        m_rightHand = rightHand;
    }

    protected override void Awake()
    {
        base.Awake();
        Debug.Log("m_player: " + m_player.Name);
        Debug.Log("m_leftHand: " + m_leftHand.HandedType);
        Debug.Log("m_rightHand: " + m_rightHand.HandedType);
        m_attack.ShowDamage();
    }

    public override void OnActionChange(HandedType handedType, ActionType actionType)
    {
        Debug.Log($"PlayerController OnActionChange: handedType={handedType}, actionType={actionType}");
    }
}
