﻿using UnityEngine;

public class TestAction2 : ActionMonoBehaviour
{
    public override void OnActionChange(HandedType handedType, ActionType actionType)
    {
        Debug.Log($"TestAction2 OnActionChange: handedType={handedType}, actionType={actionType}");
    }
}