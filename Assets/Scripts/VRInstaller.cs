﻿using Zenject;

public class VRInstaller : Installer<VRInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<IHand>().WithId(HandedType.Left).To<VRHand>().AsTransient().WithArguments(HandedType.Left);
        Container.Bind<IHand>().WithId(HandedType.Right).To<VRHand>().AsTransient().WithArguments(HandedType.Right);
        Container.BindInterfacesAndSelfTo<Player>().AsSingle().NonLazy();
        Container.Bind<string>().FromInstance("Zack");
    }
}