using UnityEngine;
using Zenject;

// Uncomment this if need to make variant of InputInstaller ScriptableObject
[CreateAssetMenu(fileName = "InputInstaller", menuName = "Installers/InputInstaller")]
public class InputInstaller : ScriptableObjectInstaller<InputInstaller>
{
    public override void InstallBindings()
    {
        switch (Application.platform)
        {
            case RuntimePlatform.WindowsEditor:
            case RuntimePlatform.WindowsPlayer:
                PCInstaller.Install(Container);
                break;
            
            case RuntimePlatform.Android:
                VRInstaller.Install(Container);
                break;
        }
    }
}