﻿using UnityEngine;

public class TestAction : ActionMonoBehaviour
{
    public override void OnActionChange(HandedType handedType, ActionType actionType)
    {
        Debug.Log($"TestAction OnActionChange: handedType={handedType}, actionType={actionType}");
    }
}