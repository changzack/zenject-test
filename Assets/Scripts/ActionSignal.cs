﻿public class ActionSignal
{
    public HandedType HandedType;
    public ActionType ActionType;
}

public enum ActionType
{
    Grab,
    Press,
    Click
}