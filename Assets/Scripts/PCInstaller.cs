﻿using DefaultNamespace;
using Zenject;

public class PCInstaller : Installer<PCInstaller>
{
    public override void InstallBindings()
    {
        Container.Bind<IHand>().WithId(HandedType.Left).To<PCHand>().AsTransient().WithArguments(HandedType.Left);
        Container.Bind<IHand>().WithId(HandedType.Right).To<PCHand>().AsTransient().WithArguments(HandedType.Right);

        Container.Bind<string>().FromInstance("Zack");
        Container.BindInterfacesAndSelfTo<Player>().AsSingle();
        Container.Bind<Normal>().AsSingle().NonLazy();

        SignalBusInstaller.Install(Container);
        Container.DeclareSignal<ActionSignal>();
        // Container.Bind<ActionMonoBehaviour>().FromComponentsInHierarchy().AsSingle();
        // Container.BindSignal<ActionSignal>()
        //     .ToMethod<ActionMonoBehaviour>((actionMonoBehaviour, signal) => 
        //         actionMonoBehaviour.OnActionChangeSelf(signal.HandedType, signal.ActionType)).FromResolve();
    }
}